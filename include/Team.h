#ifndef TEAM_H
#define TEAM_H

#include <iostream>
#include <vector>

using namespace std;

const int numWeeks = 17;

class Team
{
public:
    Team();
    Team(string city, string mascot);
    ~Team();

    void test();

    inline string getName()
    {
        return string(city + " " + mascot);
    }

private:
    string city;
    string mascot;
    double divWins;
    double divLosses;
    double totalWins;
    double totalLosses;
};

#endif
