#ifndef GAME_H
#define GAME_H

#include "Team.h"
#include <sstream>

using namespace std;

class Game
{
public:
    Game();
    ~Game();

    string getDate();

private:
    Team homeTeam;
    Team awayTeam;
    int homeScore;
    int awayScore;
    int year;
    int month;
    int day;
};

#endif